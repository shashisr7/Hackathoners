/**
 * 
 */
package com.hcl.hackthon.chatapplication.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hcl.hackthon.chatapplication.model.ChatMessageRequest;
import com.hcl.hackthon.chatapplication.model.ChatMessageResponse;
import com.hcl.hackthon.chatapplication.model.ChatRooms;
import com.hcl.hackthon.chatapplication.model.UserDetails;

/**
 * @author Hackathon
 *
 */
@Repository
public class ChatMessageDAOImpl implements ChatMessageDAO {  

	@Autowired
	public JdbcTemplate template;

	@Override
	public List<String> getChatRooms() {

		List<String> query = template.query("select distinct chat_room from chat_db.chat_history", new ChatRoomsRowMapper());
		return query;
	}

	@Override
	public List<ChatMessageResponse> saveChat(ChatMessageRequest chatMessageRequest) {

		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

		java.sql.Date requestedOn;
		try {
			requestedOn = new java.sql.Date(formatter.parse(LocalDateTime.now().toString()).getTime());
			
			System.out.println("parsing");
			
		} catch (ParseException e) {
			
			System.out.println("exception");
			requestedOn = new java.sql.Date(System.currentTimeMillis());
		}
		String sql = "INSERT INTO chat_db.chat_history(user_name,chat_room,chat_message,date_time) VALUES (?,?,?,?)";

		template.update(sql, chatMessageRequest.getUserName(), chatMessageRequest.getChatRoom(),
				chatMessageRequest.getChatMessage(), requestedOn);

		String sqll = "select * from chat_history ";

		return template.query(sqll, new ChatMessageRoomsRowMapper());
	}

	@Override
	public List<UserDetails> saveUser(UserDetails userdetails) {
		
		return null;
	}

	@Override
	public List<ChatMessageResponse> getChatDetails(String roomname) {
		
		
		
		String sql2 = "select * from chat_history where chat_room = ?";
		
				
				//return template.queryForList(sql2, new Object[] {roomname},new ChatMessageRoomsRowMapper());
		return template.query(sql2, new Object[] {roomname},new ChatMessageRoomsRowMapper());
				
				
	}

	@Override
	public ChatMessageRequest addchat(ChatMessageRequest chatmessagerequest) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

		java.sql.Date requestedOn;
		try {
			requestedOn = new java.sql.Date(formatter.parse(LocalDateTime.now().toString()).getTime());
			
			System.out.println("parsing");
			
		} catch (ParseException e) {
			
			System.out.println("exception");
			requestedOn = new java.sql.Date(System.currentTimeMillis());
		}
		String sql = "INSERT INTO chat_db.chat_history(user_name,chat_room,chat_message,date_time) VALUES (?,?,?,?)";

		template.update(sql, chatmessagerequest.getUserName(), chatmessagerequest.getChatRoom(),
				chatmessagerequest.getChatMessage(), requestedOn);
		return chatmessagerequest;

	}

/*	@Override
	public List<ChatRooms> getChatRooms1(ChatRooms chatroom) {
		
		String sql3 = "select distinct chat_room from chat_history";
		
		//return template.query(sql3,new ChatRoomsRowMapper());
		return template.query(sql3, new chatroom);
	}*/

}
class ChatMessageRoomsRowMapper implements RowMapper<ChatMessageResponse> {

	@Override
	public ChatMessageResponse mapRow(ResultSet rs, int arg1) throws SQLException {

		ChatMessageResponse chatMessageResponse = new ChatMessageResponse();
		chatMessageResponse.setSenderName(rs.getString("user_name"));
		chatMessageResponse.setChatRoom(rs.getString("chat_room"));
		chatMessageResponse.setChatMessage(rs.getString("chat_message"));
		chatMessageResponse.setDateTime(rs.getString("date_time"));

		return chatMessageResponse;

	}
}

class ChatRoomsRowMapper implements RowMapper<String> {

	@Override
	public String mapRow(ResultSet rs, int arg1) throws SQLException {
		return rs.getString("chat_room");
	}
/*
 class ChatRoomsRowMapper1 implements RowMapper<ChatRooms> {
	@Override
	public ChatRooms mapRow(ResultSet rs, int arg1) throws SQLException {
		ChatRooms chatrooms = new ChatRooms();
		chatrooms.setChatRoom(rs.getString("chat_room"));
		return null;
	}*/



}