/**
 * 
 */
package com.hcl.hackthon.chatapplication.model;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * @author Hackathon
 *
 */
@Component
public class ChatRooms {
	
	List<String> chatRoom;

	/**
	 * @return the chatRoom
	 */
	public List<String> getChatRoom() {
		return chatRoom;
	}

	/**
	 * @param chatRoom the chatRoom to set
	 */
	public void setChatRoom(List<String> chatRoom) {
		this.chatRoom = chatRoom;
	}
	
	
	
}
