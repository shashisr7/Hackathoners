/**
 * 
 */
package com.hcl.hackthon.chatapplication.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.hackthon.chatapplication.dao.ChatMessageDAO;
import com.hcl.hackthon.chatapplication.model.ChatMessageRequest;
import com.hcl.hackthon.chatapplication.model.ChatMessageResponse;
import com.hcl.hackthon.chatapplication.model.ChatRooms;
import com.hcl.hackthon.chatapplication.model.UserDetails;



/**
 * @author Hackathon
 *
 */
@Service
public class ChatMessageServiceImpl implements ChatMessageService {  

	@Autowired
	ChatMessageDAO chatMessageDAO;

	@Override
	public List<String> getAllChatRooms() {

		return chatMessageDAO.getChatRooms();
	}

	@Override
	public List<ChatMessageResponse> saveChat(ChatMessageRequest chatMessageRequest) {
		return chatMessageDAO.saveChat(chatMessageRequest);
	}

	@Override
	public List<UserDetails> saveUser(UserDetails userdetails) {
		
		return chatMessageDAO.saveUser(userdetails);
	}

	@Override
	public List<ChatMessageResponse> getChatDetails(String roomname) {
		
		return chatMessageDAO.getChatDetails(roomname);
	}

	@Override
	public ChatMessageRequest addchat(ChatMessageRequest chatmessagerequest) {
		
		return chatMessageDAO.addchat(chatmessagerequest);
	}

	/*@Override
	public List<ChatRooms> getAllChatRooms1(ChatRooms chatroom) {
		// TODO Auto-generated method stub
		return chatMessageDAO.getChatRooms1(chatroom);
	}*/

	

}
